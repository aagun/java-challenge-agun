CREATE TABLE users (
    id bigint NOT NULL,
    password character varying(255),
    username character varying(255)
);

ALTER TABLE ONLY users ADD CONSTRAINT users_pkey PRIMARY KEY (id);

CREATE SEQUENCE user_seq
    START WITH 1
    INCREMENT BY 1
    OWNED BY users.id;

