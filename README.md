### IST Java Challenge
## Requirements

- [Java 17](https://www.oracle.com/java/technologies/downloads/#java17)
- [Maven](https://maven.apache.org/download.cgi)
- [Docker](https://www.docker.com/get-started/)
- [Postman](https://www.postman.com/downloads/)

## Installation
#### Konfigurasi
- Setup database PostgreSQL menggunakan docker
    ```sh
      cd java-challenge-agun
      docker compose up -d
    ```

- Install dependency
    ```sh
      mvn clean install -DskipTests
    ```

- Menjalankan aplikasi
    ```sh
      mvn spring-boot:start
    ```

- Import file ```API Collection dan Evirontment Collection``` menggunakan Postman yang terdapat pada folder ```java-challenge-agun/src/main/resources/api-doc/```

## License

MIT