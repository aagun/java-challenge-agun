package ist.challenge.agun.exception;

import lombok.RequiredArgsConstructor;

public class InvalidAuthenticationException extends RuntimeException {
    public InvalidAuthenticationException() {
    }

    public InvalidAuthenticationException(String message) {
        super(message);
    }
}
