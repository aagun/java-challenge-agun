package ist.challenge.agun.exception;

public class ClientException extends RuntimeException {

    public ClientException() {
        super("failed");
    }

    public ClientException(String message) {
        super(message);

    }
}
