package ist.challenge.agun.service;

import ist.challenge.agun.dto.ResponseDto;
import ist.challenge.agun.dto.UserRequest;

public interface UserService {
    ResponseDto login(UserRequest user);

    ResponseDto register(UserRequest user);

    ResponseDto putUser(String userId, UserRequest user);

    ResponseDto getUsers();
}
