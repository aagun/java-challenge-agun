package ist.challenge.agun.service.impl;

import ist.challenge.agun.dao.UserDao;
import ist.challenge.agun.dto.ResponseDto;
import ist.challenge.agun.dto.UserRequest;
import ist.challenge.agun.dto.UserResponse;
import ist.challenge.agun.exception.ClientException;
import ist.challenge.agun.exception.InvalidAuthenticationException;
import ist.challenge.agun.exception.InvalidUsernameException;
import ist.challenge.agun.helper.ResponseDtoHelper;
import ist.challenge.agun.helper.StringHelpers;
import ist.challenge.agun.model.User;
import ist.challenge.agun.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    private final PasswordEncoder passwordEncoder;

    private void validateAvailableUsername(String username) {
        if (this.userDao.findByUsernameEqualsIgnoreCase(username).isPresent()) {
            throw new InvalidUsernameException("Username sudah terpakai");
        }
    }

    private void validateNewPassword(String newPassword, String currentPassword) {
        if (passwordEncoder.matches(newPassword, currentPassword)) {
            throw new ClientException("Password tidak boleh sama dengan password sebelumnya");
        }
    }

    private void validateCredential(UserRequest user) {
        if (!StringHelpers.hasValue(user.getUsername())) {
            throw new ClientException("Username harus diisi.");
        }

        if (!StringHelpers.hasValue(user.getPassword())) {
            throw new ClientException("Password harus diisi.");
        }
    }

    @Override
    public ResponseDto login(UserRequest user) {
        if (!StringHelpers.hasValue(user.getUsername()) || !StringHelpers.hasValue(user.getPassword())) {
            throw new ClientException("Username dan / atau password kosong");
        }

        User userDetail = this.userDao.findByUsernameEqualsIgnoreCase(user.getUsername())
                .orElseThrow(InvalidAuthenticationException::new);

        if (passwordEncoder.matches(user.getPassword(), userDetail.getPassword())) {
            return ResponseDtoHelper.ok("Sukses Login");
        }

        throw new InvalidAuthenticationException();
    }

    @Override
    public ResponseDto register(UserRequest user) {

        validateCredential(user);

        validateAvailableUsername(user.getUsername());

        String encodedPassword = passwordEncoder.encode(user.getPassword());

        this.userDao.save(
                User.builder()
                        .username(user.getUsername().toLowerCase())
                        .password(encodedPassword)
                        .build()
        );

        return ResponseDtoHelper.ok();
    }

    @Override
    public ResponseDto putUser(String userId, UserRequest user) {

        if (!StringHelpers.hasValue(userId) || !NumberUtils.isParsable(userId)) {
            throw new ClientException();
        }

        validateCredential(user);

        validateAvailableUsername(user.getUsername());

        Long id = Long.parseLong(userId);
        User currentUser = this.userDao.findById(id).orElseThrow(ClientException::new);

        validateNewPassword(user.getPassword(), currentUser.getPassword());

        currentUser.setUsername(user.getUsername().toLowerCase());
        currentUser.setPassword(passwordEncoder.encode(user.getPassword()));

        this.userDao.save(currentUser);

        return ResponseDtoHelper.ok();
    }

    @Override
    public ResponseDto getUsers() {
        List<User> users = this.userDao.findAll();
        List<UserResponse> data = users.stream().map(user -> {
            RestTemplate restTemplate = new RestTemplate();
            String url = "https://swapi.py4e.com/api/people/" + user.getId();
            ResponseEntity<UserResponse> people = restTemplate.getForEntity(url, UserResponse.class);

            UserResponse userResponse = people.getBody();
            userResponse.setId(user.getId());
            userResponse.setUsername(user.getUsername());

            return userResponse;

        }).toList();

        return ResponseDtoHelper.ok(data);
    }
}
