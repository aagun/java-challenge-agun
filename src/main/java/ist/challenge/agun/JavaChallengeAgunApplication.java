package ist.challenge.agun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaChallengeAgunApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaChallengeAgunApplication.class, args);
	}

}
