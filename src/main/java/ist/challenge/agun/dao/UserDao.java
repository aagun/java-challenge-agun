package ist.challenge.agun.dao;

import ist.challenge.agun.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

    Optional<User> findByUsernameEqualsIgnoreCase(String username);

    Optional<User> findByIdAndPassword(Long id, String password);

    Optional<User> findByUsernameEqualsIgnoreCaseAndPassword(String username, String password);
}
