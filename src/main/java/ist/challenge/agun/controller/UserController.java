package ist.challenge.agun.controller;

import ist.challenge.agun.dto.ResponseDto;
import ist.challenge.agun.dto.UserRequest;
import ist.challenge.agun.helper.ResponseDtoHelper;
import ist.challenge.agun.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user/")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping(value = "login")
    public ResponseEntity<ResponseDto> login(@RequestBody UserRequest user) {
        ResponseDto responseBody = this.userService.login(user);
        return ResponseEntity.ok(responseBody);
    }

    @PostMapping(value = "register")
    public ResponseEntity<ResponseDto> register(@RequestBody UserRequest user) {
        ResponseDto responseBody = this.userService.register(user);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(responseBody);
    }

    @PutMapping(value = "{userId}")
    public ResponseEntity<ResponseDto> putUser(@PathVariable String userId, @RequestBody UserRequest user) {
        ResponseDto responseBody = this.userService.putUser(userId, user);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(responseBody);
    }

    @GetMapping
    public ResponseEntity<ResponseDto> getUsers() {
        ResponseDto response = this.userService.getUsers();
        return ResponseEntity.ok(response);
    }

}
