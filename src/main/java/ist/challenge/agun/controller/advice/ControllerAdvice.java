package ist.challenge.agun.controller.advice;

import ist.challenge.agun.dto.ResponseDto;
import ist.challenge.agun.exception.ClientException;
import ist.challenge.agun.exception.InvalidAuthenticationException;
import ist.challenge.agun.exception.InvalidUsernameException;
import ist.challenge.agun.helper.ResponseDtoHelper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(basePackages = "ist.challenge.agun")
public class ControllerAdvice {

    @ExceptionHandler({InvalidUsernameException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseDto invalidCredentialHandler(InvalidUsernameException ex) {
        return ResponseDtoHelper.fail(ex.getMessage());
    }

    @ExceptionHandler(InvalidAuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseDto invalidAuthenticationHandler(InvalidAuthenticationException ex) {
        return ResponseDtoHelper.fail(ex.getMessage());
    }

    @ExceptionHandler(ClientException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto clientExceptionHandler(ClientException ex) {
        return ResponseDtoHelper.fail(ex.getMessage());
    }
}
