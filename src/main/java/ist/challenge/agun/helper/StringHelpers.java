package ist.challenge.agun.helper;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public class StringHelpers {
    public static Boolean hasValue(String value) {
        if (Objects.isNull(value)) {
            return false;
        }

        String val;
        val = Objects.toString(value, "");
        val = StringUtils.trimToEmpty(val);

        return StringUtils.isNotEmpty(val);

    }

}
